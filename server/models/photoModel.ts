import { Schema, model, Document } from 'mongoose';

export interface IPhoto extends Document {
  title: string;
  description: string;
  imagePath: string;
}

const photoSchema: Schema = new Schema({
  title: String,
  description: String,
  imagePath: String,
});

export default model<IPhoto>('photo', photoSchema);