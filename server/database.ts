import { connect } from 'mongoose';

async function startConnection() {
  try {
    await connect(process.env.MONGODB_URI || 'mongodb://localhost/upload_photo', {
    // await connect('mongodb://localhost/photo-gallery-db', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    });
    console.log('database is connected!');
  } catch (error) {
    console.log(error);
  }
}

export default startConnection;