import express from 'express';
import morgan from 'morgan';
import path from 'path';
import session from 'express-session';
import passport from 'passport';
import cors from 'cors';

import photoRoutes from './routes/PhotoRoutes';
import authRoutes from './routes/AuthRoutes';
import passportSetup from './libs/passportSetup';

const app = express();
passportSetup();

// settings
app.set('port', process.env.PORT || 3000);

// middlewares
app.use(cors());
app.use(morgan('dev'));
app.use(express.json());

// session
app.use(
  session({
    secret: 'mysecretsession',
    resave: false,
    saveUninitialized: false,
  })
);
// initialize passport
app.use(passport.initialize());
app.use(passport.session());

// routes
app.use('/photos', photoRoutes);
app.use('/auth', authRoutes);

// folder to put photos
app.use('/uploads', express.static(path.resolve('uploads')));

// handle production
if (process.env.NODE_ENV === 'production') {
  // static folder
  app.use(express.static('dist'));
  // tslint:disable-next-line: prefer-template
  app.use(express.static(__dirname + '/public/'));
  // handle SPA
  app.get(/.*/, (req, res) => {
    // tslint:disable-next-line: prefer-template
    const index = path.join(__dirname + '/public/index.html');
    // res.sendFile(`${__dirname}/public/index.html`)
    res.sendFile(index);
  });
}

export default app;