import { Request, Response } from 'express';
import path from 'path';
import fs from 'fs-extra';

// models
import Photo, { IPhoto } from '../models/photoModel';

class PhotoController {
  // get all
  public async getPhotos(req: Request, res: Response): Promise<Response> {
    const photos = await Photo.find();
    return res.json(photos);
  }
  // get by id
  public async getPhoto(req: Request, res: Response): Promise<Response> {
    const { id } = req.params;
    const photo = await Photo.findById(id);
    return res.json(photo);
  }
  // add
  public async createPhoto(req: Request, res: Response): Promise<Response> {
    const { title, description } = req.body;
    const newPhoto: IPhoto = new Photo({
      title,
      description,
      imagePath: req.file.path,
    });
    await newPhoto.save();

    return res.json({
      newPhoto,
      message: 'photo successfully saved',
    });
  }
  // delete
  public async deletePhoto(req: Request, res: Response): Promise<Response> {
    const { id } = req.params;
    const photo = await Photo.findByIdAndRemove(id);
    if (photo) {
      await fs.unlink(path.resolve(photo.imagePath));
    }
    return res.json({
      photo,
      message: 'photo deleted',
    });
  }
  // update
  public async updatePhoto(req: Request, res: Response): Promise<Response> {
    const { id } = req.params;
    const { title, description } = req.body;
    const updatePhoto = await Photo.findByIdAndUpdate(id, {
      title,
      description,
      // tslint:disable-next-line: align
    }, { new: true });
    return res.json({
      updatePhoto,
      message: 'photo successfully updated',
    });
  }

}

export const photoController = new PhotoController();
