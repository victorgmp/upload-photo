import { Router } from 'express';

import multer from '../libs/multerSetup';
import { photoController } from '../controllers/PhotoController';

export class PhotoRoutes {
  public router: Router = Router();

  constructor() {
    this.config();
  }

  public config(): void {
    const { getPhotos, createPhoto, getPhoto, deletePhoto, updatePhoto } = photoController ;
    // routes
    this.router.route('/')
      .get(getPhotos)
      .post(multer.single('image'), createPhoto);

    this.router.route('/:id')
      .get(getPhoto)
      .delete(deletePhoto)
      .put(updatePhoto);
  }
}

const photoRoutes = new PhotoRoutes();
export default photoRoutes.router;
