import { Router, Request, Response } from 'express';
import passport from 'passport';

// import User, { IUser } from '../models/userModel';

class AuthRoutes {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.get('/logout', (req, res) => {
      // res.send('logging out');
      req.logout();
      res.redirect('/');
    });

    this.router.get(
      '/google',
      passport.authenticate('google', {
        scope: ['profile'],
      })
    );

    this.router.get('/google/redirect', passport.authenticate('google'), (req: Request, res: Response) => {
      // res.send(req.user);
      // res.redirect('/profile/');
      res.redirect('/photo');
    });
  }
}

const authRoutes = new AuthRoutes();
export default authRoutes.router;
