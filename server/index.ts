import { config as configEnv } from 'dotenv';
import app from './app';
import startConnection from './database';

async function main() {
  // connect to mongodb
  startConnection();
  // require our environment variables
  configEnv();
  await app.listen(app.get('port'));
  console.log('server on port', app.get('port'));
}

main();