import { Document } from 'mongoose';
export interface IUser extends Document {
    username: string;
    password?: string;
    googleId: string;
    thumbnail: string;
}
declare const _default: import("mongoose").Model<IUser, {}>;
export default _default;
