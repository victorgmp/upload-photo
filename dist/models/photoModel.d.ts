import { Document } from 'mongoose';
export interface IPhoto extends Document {
    title: string;
    description: string;
    imagePath: string;
}
declare const _default: import("mongoose").Model<IPhoto, {}>;
export default _default;
