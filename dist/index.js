"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = require("dotenv");
const app_1 = __importDefault(require("./app"));
const database_1 = __importDefault(require("./database"));
function main() {
    return __awaiter(this, void 0, void 0, function* () {
        // connect to mongodb
        database_1.default();
        // require our environment variables
        dotenv_1.config();
        yield app_1.default.listen(app_1.default.get('port'));
        console.log('server on port', app_1.default.get('port'));
    });
}
main();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9zZXJ2ZXIvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7QUFBQSxtQ0FBNkM7QUFDN0MsZ0RBQXdCO0FBQ3hCLDBEQUF5QztBQUV6QyxTQUFlLElBQUk7O1FBQ2pCLHFCQUFxQjtRQUNyQixrQkFBZSxFQUFFLENBQUM7UUFDbEIsb0NBQW9DO1FBQ3BDLGVBQVMsRUFBRSxDQUFDO1FBQ1osTUFBTSxhQUFHLENBQUMsTUFBTSxDQUFDLGFBQUcsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztRQUNsQyxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLGFBQUcsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztJQUNqRCxDQUFDO0NBQUE7QUFFRCxJQUFJLEVBQUUsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGNvbmZpZyBhcyBjb25maWdFbnYgfSBmcm9tICdkb3RlbnYnO1xuaW1wb3J0IGFwcCBmcm9tICcuL2FwcCc7XG5pbXBvcnQgc3RhcnRDb25uZWN0aW9uIGZyb20gJy4vZGF0YWJhc2UnO1xuXG5hc3luYyBmdW5jdGlvbiBtYWluKCkge1xuICAvLyBjb25uZWN0IHRvIG1vbmdvZGJcbiAgc3RhcnRDb25uZWN0aW9uKCk7XG4gIC8vIHJlcXVpcmUgb3VyIGVudmlyb25tZW50IHZhcmlhYmxlc1xuICBjb25maWdFbnYoKTtcbiAgYXdhaXQgYXBwLmxpc3RlbihhcHAuZ2V0KCdwb3J0JykpO1xuICBjb25zb2xlLmxvZygnc2VydmVyIG9uIHBvcnQnLCBhcHAuZ2V0KCdwb3J0JykpO1xufVxuXG5tYWluKCk7Il19