"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const multerSetup_1 = __importDefault(require("../libs/multerSetup"));
const PhotoController_1 = require("../controllers/PhotoController");
class PhotoRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        const { getPhotos, createPhoto, getPhoto, deletePhoto, updatePhoto } = PhotoController_1.photoController;
        // routes
        this.router.route('/')
            .get(getPhotos)
            .post(multerSetup_1.default.single('image'), createPhoto);
        this.router.route('/:id')
            .get(getPhoto)
            .delete(deletePhoto)
            .put(updatePhoto);
    }
}
exports.PhotoRoutes = PhotoRoutes;
const photoRoutes = new PhotoRoutes();
exports.default = photoRoutes.router;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUGhvdG9Sb3V0ZXMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zZXJ2ZXIvcm91dGVzL1Bob3RvUm91dGVzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEscUNBQWlDO0FBRWpDLHNFQUF5QztBQUN6QyxvRUFBaUU7QUFFakUsTUFBYSxXQUFXO0lBR3RCO1FBRk8sV0FBTSxHQUFXLGdCQUFNLEVBQUUsQ0FBQztRQUcvQixJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7SUFDaEIsQ0FBQztJQUVNLE1BQU07UUFDWCxNQUFNLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBRSxRQUFRLEVBQUUsV0FBVyxFQUFFLFdBQVcsRUFBRSxHQUFHLGlDQUFlLENBQUU7UUFDeEYsU0FBUztRQUNULElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQzthQUNuQixHQUFHLENBQUMsU0FBUyxDQUFDO2FBQ2QsSUFBSSxDQUFDLHFCQUFNLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxFQUFFLFdBQVcsQ0FBQyxDQUFDO1FBRTdDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQzthQUN0QixHQUFHLENBQUMsUUFBUSxDQUFDO2FBQ2IsTUFBTSxDQUFDLFdBQVcsQ0FBQzthQUNuQixHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDdEIsQ0FBQztDQUNGO0FBbkJELGtDQW1CQztBQUVELE1BQU0sV0FBVyxHQUFHLElBQUksV0FBVyxFQUFFLENBQUM7QUFDdEMsa0JBQWUsV0FBVyxDQUFDLE1BQU0sQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJvdXRlciB9IGZyb20gJ2V4cHJlc3MnO1xuXG5pbXBvcnQgbXVsdGVyIGZyb20gJy4uL2xpYnMvbXVsdGVyU2V0dXAnO1xuaW1wb3J0IHsgcGhvdG9Db250cm9sbGVyIH0gZnJvbSAnLi4vY29udHJvbGxlcnMvUGhvdG9Db250cm9sbGVyJztcblxuZXhwb3J0IGNsYXNzIFBob3RvUm91dGVzIHtcbiAgcHVibGljIHJvdXRlcjogUm91dGVyID0gUm91dGVyKCk7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy5jb25maWcoKTtcbiAgfVxuXG4gIHB1YmxpYyBjb25maWcoKTogdm9pZCB7XG4gICAgY29uc3QgeyBnZXRQaG90b3MsIGNyZWF0ZVBob3RvLCBnZXRQaG90bywgZGVsZXRlUGhvdG8sIHVwZGF0ZVBob3RvIH0gPSBwaG90b0NvbnRyb2xsZXIgO1xuICAgIC8vIHJvdXRlc1xuICAgIHRoaXMucm91dGVyLnJvdXRlKCcvJylcbiAgICAgIC5nZXQoZ2V0UGhvdG9zKVxuICAgICAgLnBvc3QobXVsdGVyLnNpbmdsZSgnaW1hZ2UnKSwgY3JlYXRlUGhvdG8pO1xuXG4gICAgdGhpcy5yb3V0ZXIucm91dGUoJy86aWQnKVxuICAgICAgLmdldChnZXRQaG90bylcbiAgICAgIC5kZWxldGUoZGVsZXRlUGhvdG8pXG4gICAgICAucHV0KHVwZGF0ZVBob3RvKTtcbiAgfVxufVxuXG5jb25zdCBwaG90b1JvdXRlcyA9IG5ldyBQaG90b1JvdXRlcygpO1xuZXhwb3J0IGRlZmF1bHQgcGhvdG9Sb3V0ZXMucm91dGVyO1xuIl19