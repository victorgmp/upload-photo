"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const passport_1 = __importDefault(require("passport"));
// import User, { IUser } from '../models/userModel';
class AuthRoutes {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    routes() {
        this.router.get('/logout', (req, res) => {
            // res.send('logging out');
            req.logout();
            res.redirect('/');
        });
        this.router.get('/google', passport_1.default.authenticate('google', {
            scope: ['profile'],
        }));
        this.router.get('/google/redirect', passport_1.default.authenticate('google'), (req, res) => {
            // res.send(req.user);
            // res.redirect('/profile/');
            res.redirect('/photo');
        });
    }
}
const authRoutes = new AuthRoutes();
exports.default = authRoutes.router;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQXV0aFJvdXRlcy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3NlcnZlci9yb3V0ZXMvQXV0aFJvdXRlcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLHFDQUFvRDtBQUNwRCx3REFBZ0M7QUFFaEMscURBQXFEO0FBRXJELE1BQU0sVUFBVTtJQUdkO1FBQ0UsSUFBSSxDQUFDLE1BQU0sR0FBRyxnQkFBTSxFQUFFLENBQUM7UUFDdkIsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQ2hCLENBQUM7SUFFRCxNQUFNO1FBQ0osSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxFQUFFO1lBQ3RDLDJCQUEyQjtZQUMzQixHQUFHLENBQUMsTUFBTSxFQUFFLENBQUM7WUFDYixHQUFHLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3BCLENBQUMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQ2IsU0FBUyxFQUNULGtCQUFRLENBQUMsWUFBWSxDQUFDLFFBQVEsRUFBRTtZQUM5QixLQUFLLEVBQUUsQ0FBQyxTQUFTLENBQUM7U0FDbkIsQ0FBQyxDQUNILENBQUM7UUFFRixJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsRUFBRSxrQkFBUSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLEdBQVksRUFBRSxHQUFhLEVBQUUsRUFBRTtZQUNuRyxzQkFBc0I7WUFDdEIsNkJBQTZCO1lBQzdCLEdBQUcsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDekIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0NBQ0Y7QUFFRCxNQUFNLFVBQVUsR0FBRyxJQUFJLFVBQVUsRUFBRSxDQUFDO0FBQ3BDLGtCQUFlLFVBQVUsQ0FBQyxNQUFNLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBSb3V0ZXIsIFJlcXVlc3QsIFJlc3BvbnNlIH0gZnJvbSAnZXhwcmVzcyc7XG5pbXBvcnQgcGFzc3BvcnQgZnJvbSAncGFzc3BvcnQnO1xuXG4vLyBpbXBvcnQgVXNlciwgeyBJVXNlciB9IGZyb20gJy4uL21vZGVscy91c2VyTW9kZWwnO1xuXG5jbGFzcyBBdXRoUm91dGVzIHtcbiAgcm91dGVyOiBSb3V0ZXI7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy5yb3V0ZXIgPSBSb3V0ZXIoKTtcbiAgICB0aGlzLnJvdXRlcygpO1xuICB9XG5cbiAgcm91dGVzKCkge1xuICAgIHRoaXMucm91dGVyLmdldCgnL2xvZ291dCcsIChyZXEsIHJlcykgPT4ge1xuICAgICAgLy8gcmVzLnNlbmQoJ2xvZ2dpbmcgb3V0Jyk7XG4gICAgICByZXEubG9nb3V0KCk7XG4gICAgICByZXMucmVkaXJlY3QoJy8nKTtcbiAgICB9KTtcblxuICAgIHRoaXMucm91dGVyLmdldChcbiAgICAgICcvZ29vZ2xlJyxcbiAgICAgIHBhc3Nwb3J0LmF1dGhlbnRpY2F0ZSgnZ29vZ2xlJywge1xuICAgICAgICBzY29wZTogWydwcm9maWxlJ10sXG4gICAgICB9KVxuICAgICk7XG5cbiAgICB0aGlzLnJvdXRlci5nZXQoJy9nb29nbGUvcmVkaXJlY3QnLCBwYXNzcG9ydC5hdXRoZW50aWNhdGUoJ2dvb2dsZScpLCAocmVxOiBSZXF1ZXN0LCByZXM6IFJlc3BvbnNlKSA9PiB7XG4gICAgICAvLyByZXMuc2VuZChyZXEudXNlcik7XG4gICAgICAvLyByZXMucmVkaXJlY3QoJy9wcm9maWxlLycpO1xuICAgICAgcmVzLnJlZGlyZWN0KCcvcGhvdG8nKTtcbiAgICB9KTtcbiAgfVxufVxuXG5jb25zdCBhdXRoUm91dGVzID0gbmV3IEF1dGhSb3V0ZXMoKTtcbmV4cG9ydCBkZWZhdWx0IGF1dGhSb3V0ZXMucm91dGVyO1xuIl19