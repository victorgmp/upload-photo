"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
function startConnection() {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield mongoose_1.connect(process.env.MONGODB_URI || 'mongodb://localhost/upload_photo', {
                // await connect('mongodb://localhost/photo-gallery-db', {
                useNewUrlParser: true,
                useUnifiedTopology: true,
                useFindAndModify: false,
            });
            console.log('database is connected!');
        }
        catch (error) {
            console.log(error);
        }
    });
}
exports.default = startConnection;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YWJhc2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9zZXJ2ZXIvZGF0YWJhc2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSx1Q0FBbUM7QUFFbkMsU0FBZSxlQUFlOztRQUM1QixJQUFJO1lBQ0YsTUFBTSxrQkFBTyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxJQUFJLGtDQUFrQyxFQUFFO2dCQUM3RSwwREFBMEQ7Z0JBQ3hELGVBQWUsRUFBRSxJQUFJO2dCQUNyQixrQkFBa0IsRUFBRSxJQUFJO2dCQUN4QixnQkFBZ0IsRUFBRSxLQUFLO2FBQ3hCLENBQUMsQ0FBQztZQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsd0JBQXdCLENBQUMsQ0FBQztTQUN2QztRQUFDLE9BQU8sS0FBSyxFQUFFO1lBQ2QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNwQjtJQUNILENBQUM7Q0FBQTtBQUVELGtCQUFlLGVBQWUsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGNvbm5lY3QgfSBmcm9tICdtb25nb29zZSc7XG5cbmFzeW5jIGZ1bmN0aW9uIHN0YXJ0Q29ubmVjdGlvbigpIHtcbiAgdHJ5IHtcbiAgICBhd2FpdCBjb25uZWN0KHByb2Nlc3MuZW52Lk1PTkdPREJfVVJJIHx8ICdtb25nb2RiOi8vbG9jYWxob3N0L3VwbG9hZF9waG90bycsIHtcbiAgICAvLyBhd2FpdCBjb25uZWN0KCdtb25nb2RiOi8vbG9jYWxob3N0L3Bob3RvLWdhbGxlcnktZGInLCB7XG4gICAgICB1c2VOZXdVcmxQYXJzZXI6IHRydWUsXG4gICAgICB1c2VVbmlmaWVkVG9wb2xvZ3k6IHRydWUsXG4gICAgICB1c2VGaW5kQW5kTW9kaWZ5OiBmYWxzZSxcbiAgICB9KTtcbiAgICBjb25zb2xlLmxvZygnZGF0YWJhc2UgaXMgY29ubmVjdGVkIScpO1xuICB9IGNhdGNoIChlcnJvcikge1xuICAgIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBzdGFydENvbm5lY3Rpb247Il19