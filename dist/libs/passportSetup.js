"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const passport_1 = __importDefault(require("passport"));
const passport_google_oauth20_1 = require("passport-google-oauth20");
const dotenv_1 = require("dotenv");
// require our environment variables
dotenv_1.config();
const userModel_1 = __importDefault(require("../models/userModel"));
const passportSetup = () => {
    // Stores user in session
    passport_1.default.serializeUser((user, done) => {
        done(null, user.id);
    });
    // Retrive user from session
    passport_1.default.deserializeUser((id, done) => __awaiter(void 0, void 0, void 0, function* () {
        yield userModel_1.default.findById(id).then((user) => {
            done(null, user);
        });
    }));
    // google strategy
    passport_1.default.use(new passport_google_oauth20_1.Strategy({
        // options for the google strategy
        clientID: process.env.GOOGLE_CLIENT_ID,
        clientSecret: process.env.GOOGLE_CLIENT_SECRET,
        callbackURL: '/auth/google/redirect',
    }, (accessToken, refreshToken, profile, done) => __awaiter(void 0, void 0, void 0, function* () {
        // check if user already exist in our db
        // console.log(profile);
        const currentUser = yield userModel_1.default.findOne({ googleId: profile.id });
        try {
            if (currentUser) {
                // already have the user
                console.log('user is: ', currentUser);
                yield done(null, currentUser);
            }
            else {
                // if not, create user in our db
                const newUser = new userModel_1.default({
                    username: profile.displayName,
                    googleId: profile.id,
                    thumbnail: profile._json.picture,
                });
                yield newUser.save();
                console.log(`new user created: '${newUser}`);
                yield done(null, newUser);
            }
        }
        catch (err) {
            console.log(err);
        }
    })));
};
exports.default = passportSetup;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFzc3BvcnRTZXR1cC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3NlcnZlci9saWJzL3Bhc3Nwb3J0U2V0dXAudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7QUFBQSx3REFBZ0M7QUFDaEMscUVBQThGO0FBQzlGLG1DQUE2QztBQUM3QyxvQ0FBb0M7QUFDcEMsZUFBUyxFQUFFLENBQUM7QUFDWixvRUFBa0Q7QUFFbEQsTUFBTSxhQUFhLEdBQUcsR0FBRyxFQUFFO0lBQ3pCLHlCQUF5QjtJQUN6QixrQkFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLElBQVcsRUFBRSxJQUFTLEVBQUUsRUFBRTtRQUNoRCxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUN0QixDQUFDLENBQUMsQ0FBQztJQUNILDRCQUE0QjtJQUM1QixrQkFBUSxDQUFDLGVBQWUsQ0FBQyxDQUFPLEVBQVUsRUFBRSxJQUFTLEVBQUUsRUFBRTtRQUN2RCxNQUFNLG1CQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQVcsRUFBRSxFQUFFO1lBQzNDLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDbkIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDLENBQUEsQ0FBQyxDQUFDO0lBQ0gsa0JBQWtCO0lBQ2xCLGtCQUFRLENBQUMsR0FBRyxDQUNWLElBQUksa0NBQWMsQ0FDaEI7UUFDRSxrQ0FBa0M7UUFDbEMsUUFBUSxFQUFFLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCO1FBQ3RDLFlBQVksRUFBRSxPQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQjtRQUM5QyxXQUFXLEVBQUUsdUJBQXVCO0tBQ3JDLEVBQ0QsQ0FBTyxXQUFtQixFQUFFLFlBQW9CLEVBQUUsT0FBZ0IsRUFBRSxJQUFvQixFQUFFLEVBQUU7UUFDMUYsd0NBQXdDO1FBQ3hDLHdCQUF3QjtRQUN4QixNQUFNLFdBQVcsR0FBVSxNQUFNLG1CQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsUUFBUSxFQUFFLE9BQU8sQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQ3hFLElBQUk7WUFDRixJQUFJLFdBQVcsRUFBRTtnQkFDZix3QkFBd0I7Z0JBQ3hCLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLFdBQVcsQ0FBQyxDQUFDO2dCQUN0QyxNQUFNLElBQUksQ0FBQyxJQUFJLEVBQUUsV0FBVyxDQUFDLENBQUM7YUFDL0I7aUJBQU07Z0JBQ0wsZ0NBQWdDO2dCQUNoQyxNQUFNLE9BQU8sR0FBVSxJQUFJLG1CQUFJLENBQUM7b0JBQzlCLFFBQVEsRUFBRSxPQUFPLENBQUMsV0FBVztvQkFDN0IsUUFBUSxFQUFFLE9BQU8sQ0FBQyxFQUFFO29CQUNwQixTQUFTLEVBQUUsT0FBTyxDQUFDLEtBQUssQ0FBQyxPQUFPO2lCQUNqQyxDQUFDLENBQUM7Z0JBQ0gsTUFBTSxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQ3JCLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0JBQXNCLE9BQU8sRUFBRSxDQUFDLENBQUM7Z0JBQzdDLE1BQU0sSUFBSSxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQzthQUMzQjtTQUNGO1FBQUMsT0FBTyxHQUFHLEVBQUU7WUFDWixPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQ2xCO0lBQ0gsQ0FBQyxDQUFBLENBQ0YsQ0FDRixDQUFDO0FBRUosQ0FBQyxDQUFDO0FBRUYsa0JBQWUsYUFBYSxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHBhc3Nwb3J0IGZyb20gJ3Bhc3Nwb3J0JztcbmltcG9ydCB7IFN0cmF0ZWd5IGFzIEdvb2dsZVN0cmF0ZWd5LCBQcm9maWxlLCBWZXJpZnlDYWxsYmFjayB9IGZyb20gJ3Bhc3Nwb3J0LWdvb2dsZS1vYXV0aDIwJztcbmltcG9ydCB7IGNvbmZpZyBhcyBjb25maWdFbnYgfSBmcm9tICdkb3RlbnYnO1xuLy8gcmVxdWlyZSBvdXIgZW52aXJvbm1lbnQgdmFyaWFibGVzXG5jb25maWdFbnYoKTtcbmltcG9ydCBVc2VyLCB7IElVc2VyIH0gZnJvbSAnLi4vbW9kZWxzL3VzZXJNb2RlbCc7XG5cbmNvbnN0IHBhc3Nwb3J0U2V0dXAgPSAoKSA9PiB7XG4gIC8vIFN0b3JlcyB1c2VyIGluIHNlc3Npb25cbiAgcGFzc3BvcnQuc2VyaWFsaXplVXNlcigodXNlcjogSVVzZXIsIGRvbmU6IGFueSkgPT4ge1xuICAgIGRvbmUobnVsbCwgdXNlci5pZCk7XG4gIH0pO1xuICAvLyBSZXRyaXZlIHVzZXIgZnJvbSBzZXNzaW9uXG4gIHBhc3Nwb3J0LmRlc2VyaWFsaXplVXNlcihhc3luYyAoaWQ6IHN0cmluZywgZG9uZTogYW55KSA9PiB7XG4gICAgYXdhaXQgVXNlci5maW5kQnlJZChpZCkudGhlbigodXNlcjogSVVzZXIpID0+IHtcbiAgICAgIGRvbmUobnVsbCwgdXNlcik7XG4gICAgfSk7XG4gIH0pO1xuICAvLyBnb29nbGUgc3RyYXRlZ3lcbiAgcGFzc3BvcnQudXNlKFxuICAgIG5ldyBHb29nbGVTdHJhdGVneShcbiAgICAgIHtcbiAgICAgICAgLy8gb3B0aW9ucyBmb3IgdGhlIGdvb2dsZSBzdHJhdGVneVxuICAgICAgICBjbGllbnRJRDogcHJvY2Vzcy5lbnYuR09PR0xFX0NMSUVOVF9JRCxcbiAgICAgICAgY2xpZW50U2VjcmV0OiBwcm9jZXNzLmVudi5HT09HTEVfQ0xJRU5UX1NFQ1JFVCxcbiAgICAgICAgY2FsbGJhY2tVUkw6ICcvYXV0aC9nb29nbGUvcmVkaXJlY3QnLFxuICAgICAgfSxcbiAgICAgIGFzeW5jIChhY2Nlc3NUb2tlbjogc3RyaW5nLCByZWZyZXNoVG9rZW46IHN0cmluZywgcHJvZmlsZTogUHJvZmlsZSwgZG9uZTogVmVyaWZ5Q2FsbGJhY2spID0+IHtcbiAgICAgICAgLy8gY2hlY2sgaWYgdXNlciBhbHJlYWR5IGV4aXN0IGluIG91ciBkYlxuICAgICAgICAvLyBjb25zb2xlLmxvZyhwcm9maWxlKTtcbiAgICAgICAgY29uc3QgY3VycmVudFVzZXI6IElVc2VyID0gYXdhaXQgVXNlci5maW5kT25lKHsgZ29vZ2xlSWQ6IHByb2ZpbGUuaWQgfSk7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgaWYgKGN1cnJlbnRVc2VyKSB7XG4gICAgICAgICAgICAvLyBhbHJlYWR5IGhhdmUgdGhlIHVzZXJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCd1c2VyIGlzOiAnLCBjdXJyZW50VXNlcik7XG4gICAgICAgICAgICBhd2FpdCBkb25lKG51bGwsIGN1cnJlbnRVc2VyKTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgLy8gaWYgbm90LCBjcmVhdGUgdXNlciBpbiBvdXIgZGJcbiAgICAgICAgICAgIGNvbnN0IG5ld1VzZXI6IElVc2VyID0gbmV3IFVzZXIoe1xuICAgICAgICAgICAgICB1c2VybmFtZTogcHJvZmlsZS5kaXNwbGF5TmFtZSxcbiAgICAgICAgICAgICAgZ29vZ2xlSWQ6IHByb2ZpbGUuaWQsXG4gICAgICAgICAgICAgIHRodW1ibmFpbDogcHJvZmlsZS5fanNvbi5waWN0dXJlLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBhd2FpdCBuZXdVc2VyLnNhdmUoKTtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGBuZXcgdXNlciBjcmVhdGVkOiAnJHtuZXdVc2VyfWApO1xuICAgICAgICAgICAgYXdhaXQgZG9uZShudWxsLCBuZXdVc2VyKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgICAgIGNvbnNvbGUubG9nKGVycik7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICApXG4gICk7XG5cbn07XG5cbmV4cG9ydCBkZWZhdWx0IHBhc3Nwb3J0U2V0dXA7XG4iXX0=