import { Request, Response } from 'express';
declare class PhotoController {
    getPhotos(req: Request, res: Response): Promise<Response>;
    getPhoto(req: Request, res: Response): Promise<Response>;
    createPhoto(req: Request, res: Response): Promise<Response>;
    deletePhoto(req: Request, res: Response): Promise<Response>;
    updatePhoto(req: Request, res: Response): Promise<Response>;
}
export declare const photoController: PhotoController;
export {};
