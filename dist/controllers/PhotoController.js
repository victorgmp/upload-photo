"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = __importDefault(require("path"));
const fs_extra_1 = __importDefault(require("fs-extra"));
// models
const photoModel_1 = __importDefault(require("../models/photoModel"));
class PhotoController {
    // get all
    getPhotos(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const photos = yield photoModel_1.default.find();
            return res.json(photos);
        });
    }
    // get by id
    getPhoto(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            const photo = yield photoModel_1.default.findById(id);
            return res.json(photo);
        });
    }
    // add
    createPhoto(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { title, description } = req.body;
            const newPhoto = new photoModel_1.default({
                title,
                description,
                imagePath: req.file.path,
            });
            yield newPhoto.save();
            return res.json({
                newPhoto,
                message: 'photo successfully saved',
            });
        });
    }
    // delete
    deletePhoto(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            const photo = yield photoModel_1.default.findByIdAndRemove(id);
            if (photo) {
                yield fs_extra_1.default.unlink(path_1.default.resolve(photo.imagePath));
            }
            return res.json({
                photo,
                message: 'photo deleted',
            });
        });
    }
    // update
    updatePhoto(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            const { title, description } = req.body;
            const updatePhoto = yield photoModel_1.default.findByIdAndUpdate(id, {
                title,
                description,
            }, { new: true });
            return res.json({
                updatePhoto,
                message: 'photo successfully updated',
            });
        });
    }
}
exports.photoController = new PhotoController();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUGhvdG9Db250cm9sbGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc2VydmVyL2NvbnRyb2xsZXJzL1Bob3RvQ29udHJvbGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7OztBQUNBLGdEQUF3QjtBQUN4Qix3REFBMEI7QUFFMUIsU0FBUztBQUNULHNFQUFxRDtBQUVyRCxNQUFNLGVBQWU7SUFDbkIsVUFBVTtJQUNHLFNBQVMsQ0FBQyxHQUFZLEVBQUUsR0FBYTs7WUFDaEQsTUFBTSxNQUFNLEdBQUcsTUFBTSxvQkFBSyxDQUFDLElBQUksRUFBRSxDQUFDO1lBQ2xDLE9BQU8sR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMxQixDQUFDO0tBQUE7SUFDRCxZQUFZO0lBQ0MsUUFBUSxDQUFDLEdBQVksRUFBRSxHQUFhOztZQUMvQyxNQUFNLEVBQUUsRUFBRSxFQUFFLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQztZQUMxQixNQUFNLEtBQUssR0FBRyxNQUFNLG9CQUFLLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ3ZDLE9BQU8sR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN6QixDQUFDO0tBQUE7SUFDRCxNQUFNO0lBQ08sV0FBVyxDQUFDLEdBQVksRUFBRSxHQUFhOztZQUNsRCxNQUFNLEVBQUUsS0FBSyxFQUFFLFdBQVcsRUFBRSxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUM7WUFDeEMsTUFBTSxRQUFRLEdBQVcsSUFBSSxvQkFBSyxDQUFDO2dCQUNqQyxLQUFLO2dCQUNMLFdBQVc7Z0JBQ1gsU0FBUyxFQUFFLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSTthQUN6QixDQUFDLENBQUM7WUFDSCxNQUFNLFFBQVEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUV0QixPQUFPLEdBQUcsQ0FBQyxJQUFJLENBQUM7Z0JBQ2QsUUFBUTtnQkFDUixPQUFPLEVBQUUsMEJBQTBCO2FBQ3BDLENBQUMsQ0FBQztRQUNMLENBQUM7S0FBQTtJQUNELFNBQVM7SUFDSSxXQUFXLENBQUMsR0FBWSxFQUFFLEdBQWE7O1lBQ2xELE1BQU0sRUFBRSxFQUFFLEVBQUUsR0FBRyxHQUFHLENBQUMsTUFBTSxDQUFDO1lBQzFCLE1BQU0sS0FBSyxHQUFHLE1BQU0sb0JBQUssQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUNoRCxJQUFJLEtBQUssRUFBRTtnQkFDVCxNQUFNLGtCQUFFLENBQUMsTUFBTSxDQUFDLGNBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7YUFDaEQ7WUFDRCxPQUFPLEdBQUcsQ0FBQyxJQUFJLENBQUM7Z0JBQ2QsS0FBSztnQkFDTCxPQUFPLEVBQUUsZUFBZTthQUN6QixDQUFDLENBQUM7UUFDTCxDQUFDO0tBQUE7SUFDRCxTQUFTO0lBQ0ksV0FBVyxDQUFDLEdBQVksRUFBRSxHQUFhOztZQUNsRCxNQUFNLEVBQUUsRUFBRSxFQUFFLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQztZQUMxQixNQUFNLEVBQUUsS0FBSyxFQUFFLFdBQVcsRUFBRSxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUM7WUFDeEMsTUFBTSxXQUFXLEdBQUcsTUFBTSxvQkFBSyxDQUFDLGlCQUFpQixDQUFDLEVBQUUsRUFBRTtnQkFDcEQsS0FBSztnQkFDTCxXQUFXO2FBRVosRUFBRSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO1lBQ2xCLE9BQU8sR0FBRyxDQUFDLElBQUksQ0FBQztnQkFDZCxXQUFXO2dCQUNYLE9BQU8sRUFBRSw0QkFBNEI7YUFDdEMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQztLQUFBO0NBRUY7QUFFWSxRQUFBLGVBQWUsR0FBRyxJQUFJLGVBQWUsRUFBRSxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUmVxdWVzdCwgUmVzcG9uc2UgfSBmcm9tICdleHByZXNzJztcbmltcG9ydCBwYXRoIGZyb20gJ3BhdGgnO1xuaW1wb3J0IGZzIGZyb20gJ2ZzLWV4dHJhJztcblxuLy8gbW9kZWxzXG5pbXBvcnQgUGhvdG8sIHsgSVBob3RvIH0gZnJvbSAnLi4vbW9kZWxzL3Bob3RvTW9kZWwnO1xuXG5jbGFzcyBQaG90b0NvbnRyb2xsZXIge1xuICAvLyBnZXQgYWxsXG4gIHB1YmxpYyBhc3luYyBnZXRQaG90b3MocmVxOiBSZXF1ZXN0LCByZXM6IFJlc3BvbnNlKTogUHJvbWlzZTxSZXNwb25zZT4ge1xuICAgIGNvbnN0IHBob3RvcyA9IGF3YWl0IFBob3RvLmZpbmQoKTtcbiAgICByZXR1cm4gcmVzLmpzb24ocGhvdG9zKTtcbiAgfVxuICAvLyBnZXQgYnkgaWRcbiAgcHVibGljIGFzeW5jIGdldFBob3RvKHJlcTogUmVxdWVzdCwgcmVzOiBSZXNwb25zZSk6IFByb21pc2U8UmVzcG9uc2U+IHtcbiAgICBjb25zdCB7IGlkIH0gPSByZXEucGFyYW1zO1xuICAgIGNvbnN0IHBob3RvID0gYXdhaXQgUGhvdG8uZmluZEJ5SWQoaWQpO1xuICAgIHJldHVybiByZXMuanNvbihwaG90byk7XG4gIH1cbiAgLy8gYWRkXG4gIHB1YmxpYyBhc3luYyBjcmVhdGVQaG90byhyZXE6IFJlcXVlc3QsIHJlczogUmVzcG9uc2UpOiBQcm9taXNlPFJlc3BvbnNlPiB7XG4gICAgY29uc3QgeyB0aXRsZSwgZGVzY3JpcHRpb24gfSA9IHJlcS5ib2R5O1xuICAgIGNvbnN0IG5ld1Bob3RvOiBJUGhvdG8gPSBuZXcgUGhvdG8oe1xuICAgICAgdGl0bGUsXG4gICAgICBkZXNjcmlwdGlvbixcbiAgICAgIGltYWdlUGF0aDogcmVxLmZpbGUucGF0aCxcbiAgICB9KTtcbiAgICBhd2FpdCBuZXdQaG90by5zYXZlKCk7XG5cbiAgICByZXR1cm4gcmVzLmpzb24oe1xuICAgICAgbmV3UGhvdG8sXG4gICAgICBtZXNzYWdlOiAncGhvdG8gc3VjY2Vzc2Z1bGx5IHNhdmVkJyxcbiAgICB9KTtcbiAgfVxuICAvLyBkZWxldGVcbiAgcHVibGljIGFzeW5jIGRlbGV0ZVBob3RvKHJlcTogUmVxdWVzdCwgcmVzOiBSZXNwb25zZSk6IFByb21pc2U8UmVzcG9uc2U+IHtcbiAgICBjb25zdCB7IGlkIH0gPSByZXEucGFyYW1zO1xuICAgIGNvbnN0IHBob3RvID0gYXdhaXQgUGhvdG8uZmluZEJ5SWRBbmRSZW1vdmUoaWQpO1xuICAgIGlmIChwaG90bykge1xuICAgICAgYXdhaXQgZnMudW5saW5rKHBhdGgucmVzb2x2ZShwaG90by5pbWFnZVBhdGgpKTtcbiAgICB9XG4gICAgcmV0dXJuIHJlcy5qc29uKHtcbiAgICAgIHBob3RvLFxuICAgICAgbWVzc2FnZTogJ3Bob3RvIGRlbGV0ZWQnLFxuICAgIH0pO1xuICB9XG4gIC8vIHVwZGF0ZVxuICBwdWJsaWMgYXN5bmMgdXBkYXRlUGhvdG8ocmVxOiBSZXF1ZXN0LCByZXM6IFJlc3BvbnNlKTogUHJvbWlzZTxSZXNwb25zZT4ge1xuICAgIGNvbnN0IHsgaWQgfSA9IHJlcS5wYXJhbXM7XG4gICAgY29uc3QgeyB0aXRsZSwgZGVzY3JpcHRpb24gfSA9IHJlcS5ib2R5O1xuICAgIGNvbnN0IHVwZGF0ZVBob3RvID0gYXdhaXQgUGhvdG8uZmluZEJ5SWRBbmRVcGRhdGUoaWQsIHtcbiAgICAgIHRpdGxlLFxuICAgICAgZGVzY3JpcHRpb24sXG4gICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6IGFsaWduXG4gICAgfSwgeyBuZXc6IHRydWUgfSk7XG4gICAgcmV0dXJuIHJlcy5qc29uKHtcbiAgICAgIHVwZGF0ZVBob3RvLFxuICAgICAgbWVzc2FnZTogJ3Bob3RvIHN1Y2Nlc3NmdWxseSB1cGRhdGVkJyxcbiAgICB9KTtcbiAgfVxuXG59XG5cbmV4cG9ydCBjb25zdCBwaG90b0NvbnRyb2xsZXIgPSBuZXcgUGhvdG9Db250cm9sbGVyKCk7XG4iXX0=