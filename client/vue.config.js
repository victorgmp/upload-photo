const path = require('path');

module.exports = {
  outputDir: path.resolve(__dirname, '../dist/public'),
  devServer: {
    proxy: {
      '/auth': {
        target: 'http://localhost:7002',
      },
      '/photos': {
        target: 'http://localhost:7002',
      },
      '/uploads': {
        target: 'http://localhost:7002',
      },
    },
  },
};
