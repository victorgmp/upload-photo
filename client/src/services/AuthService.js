import axios from 'axios';

const url = 'auth/';

class AuthService {
  // get photo
  static getGoogle() {
    return axios.get(`${url}google`);
    // const data = res.data;
  }
}

export default AuthService;
