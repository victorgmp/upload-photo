import axios from 'axios';

const url = 'photos/';

class PhotoService {
  // get photos
  static getPhotos() {
    return new Promise(async (resolve, reject) => {
      try {
        const res = await axios.get(url);
        const { data } = res;
        resolve(
          data.map(photo => ({
            ...photo,
          })),
        );
      } catch (err) {
        reject(err);
      }
    });
  }

  // get photo
  static getPhoto(id) {
    return axios.get(`${url}${id}`);
    // const data = res.data;
  }

  // create photo
  static insertPhoto(photo) {
    // console.log(photo);
    return axios.post(url, {
      title: photo.title,
      description: photo.description,
      imagePath: photo.imagePath,
    });
  }

  // update photo
  static updatePhoto(id, photo) {
    // console.log(photo);
    return axios.put(`${url}${id}`, {
      title: photo.title,
      description: photo.description,
    });
  }

  // delete photo
  static deletePhoto(id) {
    return axios.delete(`${url}${id}`);
  }
}

export default PhotoService;
