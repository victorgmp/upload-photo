import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Photo from './components/Photo.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: __dirname,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      // component: () => import(/* webpackChunkName: "about" */ './views/Home.vue'),
    },
    {
      path: '/photo',
      name: 'photo',
      component: Photo,
      // component: () => import(/* webpackChunkName: "about" */ './views/Photo.vue'),
    },
  ],
});
